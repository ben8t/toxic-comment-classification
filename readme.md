# Toxic Comment Classification Challenge

https://www.kaggle.com/c/jigsaw-toxic-comment-classification-challenge

----

# Docker

## Build image

`docker build -t nlp_model .`

## Run

`docker run --rm -it -v $(pwd):/app nlp_model python -m train`

# Kaggle Kernels

* https://www.kaggle.com/tunguz/logistic-regression-with-words-and-char-n-grams


